#!/usr/bin/env bash
export APP_NAME=ex-book-api-gateway-go
cp ./doc/$APP_NAME.html ./doc/index.html
python3 -m http.server 8880 --bind 127.0.0.1 --directory ./doc
