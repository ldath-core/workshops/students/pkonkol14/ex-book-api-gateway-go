FROM alpine:latest

RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY ex-book-api-gateway-go /bin/ex-book-api-gateway-go

EXPOSE 8080

ENTRYPOINT ["/bin/ex-book-api-gateway-go"]

CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080"]
