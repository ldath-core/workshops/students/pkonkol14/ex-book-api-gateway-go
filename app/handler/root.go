package handler

import (
	"errors"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/config"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"
	"strings"
	"time"
)

type Controller struct {
	RDB    *redis.Client
	Logger *logrus.Logger
	Config *config.Config
}

func (c *Controller) ProxyHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		proxy.ServeHTTP(writer, request)
	}
}

func parseAuthorizationHeader(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	authSlice := strings.Split(authHeader, " ")
	if len(authSlice) == 1 {
		return authSlice[0], nil
	} else if len(authSlice) == 2 {
		return authSlice[1], nil
	} else {
		return "", errors.New("wrong token format")
	}
}

func getApiUrlWithPath(apiUrl string, pathPart string, qMap map[string]string) (string, error) {
	u, err := url.Parse(apiUrl)
	if err != nil {
		return "", err
	}
	u.Path = path.Join(u.Path, pathPart)
	q := u.Query()
	for key, value := range qMap {
		q.Set(key, value)
	}
	u.RawQuery = q.Encode()
	return u.String(), nil
}

func getApiBody(url string, timeout time.Duration) (int, []byte, error) {
	statusCode := 500
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return statusCode, nil, err
	}
	req.Header.Set("User-Agent", "book-api-gateway")

	client := http.Client{
		Timeout: time.Second * timeout,
	}
	res, getErr := client.Do(req)
	if getErr != nil {
		return statusCode, nil, err
	}
	statusCode = res.StatusCode

	if res.Body != nil {
		defer func() {
			err = res.Body.Close()
		}()
	}

	body, readErr := io.ReadAll(res.Body)
	if readErr != nil {
		return statusCode, nil, readErr
	}
	return statusCode, body, err
}
