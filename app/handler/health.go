package handler

import (
	"context"
	"encoding/json"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/model"
	"net/http"
	"time"
)

// GetHealth will handle health get request
func (c *Controller) GetHealth(res http.ResponseWriter, _ *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	r := true
	_, err := c.RDB.Ping(ctx).Result()
	if err != nil {
		r = false
	}

	bookList, err := checkApiHealth(c.Config.API.BookList.Url)
	if err != nil {
		c.Logger.WithField("error", err).Warning("checkApiHealth for customer returned unexpected error")
	}
	bookAdmin, err := checkApiHealth(c.Config.API.BookAdmin.Url)
	if err != nil {
		c.Logger.WithField("error", err).Warning("checkApiHealth for shop returned unexpected error")
	}

	err = ResponseWriter(res, http.StatusOK, "ex-book-api-gateway-go health", model.Health{
		Alive: true,
		Redis: r,
		ApiHealth: model.ApiHealth{
			BookList:  bookList,
			BookAdmin: bookAdmin,
		},
	})
	if err != nil {
		c.Logger.Error(err)
	}
}

func checkApiHealth(apiUrl string) (bool, error) {
	fullUrl, err := getApiUrlWithPath(apiUrl, "v1/health", make(map[string]string))
	if err != nil {
		return false, err
	}
	_, body, err := getApiBody(fullUrl, time.Duration(2))
	result := model.CheckApiHealth{}
	jsonErr := json.Unmarshal(body, &result)
	if jsonErr != nil {
		return false, jsonErr
	}
	return result.Content.Alive, nil
}
