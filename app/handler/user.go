package handler

import (
	"encoding/json"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/lib"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/model"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

func (c Controller) Login(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var jwt model.JWT
	var errModel model.Error

	json.NewDecoder(r.Body).Decode(&user)

	if user.Email == "" {
		errModel.Message = "Email is missing"
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}

	if user.Password == "" {
		errModel.Message = "Password is missing"
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}

	bookAdminOutput, err := getAdminsByEmail(c.Config.API.BookAdmin.Url, user.Email)
	if err != nil {
		errModel.Message = "Internal Server Error"
		RespondWithError(w, http.StatusInternalServerError, errModel)
		return
	}
	if bookAdminOutput.Content.Count > 1 {
		errModel.Message = "Internal Server Error"
		c.Logger.Error("Broken Database")
		RespondWithError(w, http.StatusInternalServerError, errModel)
		return
	} else if bookAdminOutput.Content.Count < 1 {
		errModel.Message = "Failed to authenticate. Check your login data."
		c.Logger.Debug("User Not Found")
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}

	hashedPassword := bookAdminOutput.Content.Results[0].PasswordHash

	if checkPasswordHash(user.Password, hashedPassword) {
		token, err := lib.GenerateToken(user, c.Config)
		if err != nil {
			errModel.Message = "Failed to authenticate. Check your login data."
			c.Logger.Debug("Broken Token")
			RespondWithError(w, http.StatusBadRequest, errModel)
			return
		}
		jwt.Token = token
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Authorization", token)
		ResponseWriter(w, http.StatusOK, "JWT - token", jwt)
	} else {
		errModel.Message = "Failed to authenticate. Check your login data."
		c.Logger.Debug("Wrong Password")
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}
}

func getAdminsByEmail(apiUrl string, email string) (model.BookAdminsOutput, error) {
	qMap := map[string]string{"skip": "0", "limit": "2", "email": email}
	fullUrl, err := getApiUrlWithPath(apiUrl, "v1/admins", qMap)
	if err != nil {
		return model.BookAdminsOutput{}, err
	}
	_, body, err := getApiBody(fullUrl, time.Duration(2))
	result := model.BookAdminsOutput{}
	jsonErr := json.Unmarshal(body, &result)
	if jsonErr != nil {
		return model.BookAdminsOutput{}, jsonErr
	}
	return result, nil
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
