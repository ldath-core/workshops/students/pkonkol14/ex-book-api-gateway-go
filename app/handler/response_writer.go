package handler

import (
	"encoding/json"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/model"
	"net/http"
)

// ResponseWriter will write result in http.ResponseWriter
func ResponseWriter(w http.ResponseWriter, statusCode int, message string, data interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	httpResponse := model.NewResponse(statusCode, message, data)
	err := json.NewEncoder(w).Encode(httpResponse)
	return err
}

func RespondWithError(w http.ResponseWriter, statusCode int, error model.Error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(error)
}

func ResponseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}

func MakeGetCallToApi(apiUrl string, r *http.Request) (*http.Response, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", apiUrl, nil)
	if err != nil {
		return &http.Response{}, err
	}

	q := r.URL.Query()
	req.URL.RawQuery = q.Encode()
	resp, err := client.Do(req)
	if err != nil {
		return &http.Response{}, err
	}

	return resp, nil
}
