package handler

import (
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/model"
	"net/http"
)

func (c Controller) ProtectedEndpoint(w http.ResponseWriter, r *http.Request) {
	//ctx := r.Context()
	//c.Logger.Printf("Context uid: %v", ctx.Value("uid").(string))
	//c.Logger.Printf("Context employee: %v", ctx.Value("employee").(model.Employee))
	ResponseJSON(w, model.Response{Status: 200, Message: "Protected"})
}
