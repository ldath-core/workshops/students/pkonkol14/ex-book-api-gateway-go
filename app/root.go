package app

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/handler"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/config"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

// App has the mongo database and router instances
type App struct {
	Router *mux.Router
	RDB    *redis.Client
	Logger *logrus.Logger
	Config *config.Config
}

// ConfigAndRunApp will create and initialize App structure. App factory functions.
func ConfigAndRunApp(config *config.Config, logFormat string, corsEnabled bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat)

	app.Run(fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port), corsEnabled)
}

// Initialize the app with config
func (app *App) Initialize(ctx context.Context, config *config.Config, logFormat string) {
	// Configuration
	app.Config = config

	// Configure Logger
	app.setLogger(config, logFormat)

	// Configure Redis
	app.setRedis(config)

	// Router
	app.Router = mux.NewRouter()
	app.UseMiddleware(handler.JSONContentTypeMiddleware)
	app.setRouters()
}

func (app *App) setLogger(config *config.Config, format string) {
	app.Logger = logrus.New()
	app.Logger.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		app.Logger.SetFormatter(&logrus.JSONFormatter{})
		//app.Logger.SetReportCaller(true)
	} else {
		app.Logger.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	lvl, err := logrus.ParseLevel(config.Logger.Level)
	if err != nil {
		lvl = logrus.WarnLevel
		app.Logger.SetLevel(lvl)
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Configuration Error")
	} else {
		app.Logger.SetLevel(lvl)
	}
}

func (app *App) setRedis(config *config.Config) {
	app.RDB = redis.NewClient(&redis.Options{
		Addr:         config.Redis.Uri,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
		Password:     "", // no password set
		DB:           0,  // use default DB
	})
	app.Logger.WithFields(
		logrus.Fields{
			"RedisUri": config.Redis.Uri,
		},
	).Info("Redis configured")
}

// UseMiddleware will add global middleware in router
func (app *App) UseMiddleware(middleware mux.MiddlewareFunc) {
	app.Router.Use(middleware)
}

func (app *App) setRouters() {
	c := handler.Controller{
		RDB:    app.RDB,
		Logger: app.Logger,
		Config: app.Config,
	}
	app.Get("/v1/health", c.GetHealth)
	app.Post("/v1/login", c.Login)
	app.Get("/v1/protected", c.TokenVerifyMiddleWare(c.ProtectedEndpoint))

	// BookList ReverseProxy
	bookListUrl, err := url.Parse(c.Config.API.BookList.Url)
	if err != nil {
		app.Logger.Fatal(err)
	}
	bookListProxy := httputil.NewSingleHostReverseProxy(bookListUrl)
	app.Router.PathPrefix("/book-list/").Handler(http.StripPrefix("/book-list/", c.TokenVerifyMiddleWare(c.ProxyHandler(bookListProxy))))

	// BookAdmin ReverseProxy
	bookAdminUrl, err := url.Parse(c.Config.API.BookAdmin.Url)
	if err != nil {
		app.Logger.Fatal(err)
	}
	bookAdminProxy := httputil.NewSingleHostReverseProxy(bookAdminUrl)
	app.Router.PathPrefix("/book-admin/").Handler(http.StripPrefix("/book-admin/", c.TokenVerifyMiddleWare(c.ProxyHandler(bookAdminProxy))))

}

// Get will register Get method for an endpoint
func (app *App) Get(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("GET").Queries(queries...)
}

// Post will register Post method for an endpoint
func (app *App) Post(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("POST").Queries(queries...)
}

// Put will register Put method for an endpoint
func (app *App) Put(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PUT").Queries(queries...)
}

// Patch will register Patch method for an endpoint
func (app *App) Patch(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PATCH").Queries(queries...)
}

// Delete will register Delete method for an endpoint
func (app *App) Delete(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("DELETE").Queries(queries...)
}

// Run will start the http server on host that you pass in. host:<ip:port>
func (app *App) Run(host string, corsEnabled bool) {
	// use signals for shutdown server gracefully.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	go func() {
		if corsEnabled {
			appHandler := cors.New(cors.Options{
				AllowedOrigins:   []string{"*"},
				AllowedHeaders:   []string{"*"},
				AllowCredentials: true,
				Debug:            false,
			}).Handler(app.Router)
			app.Logger.Fatal(http.ListenAndServe(host, appHandler))
		} else {
			app.Logger.Fatal(http.ListenAndServe(host, app.Router))
		}
	}()
	app.Logger.Infof("Server is listning on %s://%s", "http", host)

	sig := <-sigs
	app.Logger.Infoln("Signal: ", sig)
}
