package lib

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/app/model"
	"gitlab.com/ldath-core/examples/ex-book-api-gateway-go/config"
	"golang.org/x/crypto/bcrypt"
	"log"
)

//func RespondWithError(w http.ResponseWriter, status int, error model.Error)  {
//	w.WriteHeader(status)
//	json.NewEncoder(w).Encode(error)
//}
//
//func ResponseJSON(w http.ResponseWriter, data interface{})  {
//	json.NewEncoder(w).Encode(data)
//}

func ComparePasswords(hashedPassword string, password []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		return false
	}

	return true
}

func GenerateToken(user model.User, c *config.Config) (string, error) {
	var err error

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": user.Email,
		"iss":   "course",
	})

	tokenString, err := token.SignedString([]byte(c.Server.JwtSecret))
	if err != nil {
		log.Fatal(err)
	}

	return tokenString, nil
}
