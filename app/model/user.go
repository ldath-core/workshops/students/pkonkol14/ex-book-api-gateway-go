package model

type User struct {
	Email             string `json:"email"`
	Password          string `json:"password"`
	ReturnSecureToken bool   `json:"returnSecureToken"`
}

type BookAdminObject struct {
	ID           string `json:"id"`
	Email        string `json:"email"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	PasswordHash string `json:"passwordHash"`
	Version      int    `json:"version"`
}

type BookAdminResults []BookAdminObject

type PaginatedBookAdminsContent struct {
	Count   int `json:"count"`
	Skip    int `json:"skip"`
	Limit   int `json:"limit"`
	Results BookAdminResults
}

type BookAdminsOutput struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Content PaginatedBookAdminsContent
}
