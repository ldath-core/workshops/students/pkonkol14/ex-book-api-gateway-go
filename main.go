/*
Copyright © 2022 Artkadiusz Tułodziecki <atulodzi@gmail.com>

*/
package main

import "gitlab.com/ldath-core/examples/ex-book-api-gateway-go/cmd"

func main() {
	cmd.Execute()
}
